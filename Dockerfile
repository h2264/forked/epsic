FROM registry.gitlab.com/ska-telescope/pst/ska-pst/ska-pst-testutils:0.5.1
USER root

ARG NB_USER=jovyan
ARG NB_UID=1001

ENV USER ${NB_USER}
ENV HOME /home/${USER}
ENV PATH /home/${USER}/.local/bin:${PATH}

RUN userdel tango
RUN useradd --create-home --home-dir /home/${USER} ${USER}
RUN usermod -u ${NB_UID} -g ${NB_UID} ${USER}

WORKDIR ${HOME}

COPY --chown=${USER}:${USER} . ./

USER ${USER}

RUN python3 -m pip install --no-cache-dir notebook jupyterlab jupyter_bokeh

RUN pip install --no-cache-dir jupyterhub

